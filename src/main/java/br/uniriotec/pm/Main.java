package br.uniriotec.pm;

// Javalin API Builder Imports
import static io.javalin.apibuilder.ApiBuilder.delete;
import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.patch;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;
// Controllers Import
import br.uniriotec.pm.controllers.UserController;
// Javalin.io Imports
import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.ReDocOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;

public class Main {

	public static void main(String[] args) {

		// new comment
		Javalin.create(config -> {
			config.registerPlugin(getConfiguredOpenApiPlugin());
			config.defaultContentType = "application/json";
		}).routes(() -> {
			// User related endpoints
			path("users", () -> {
				get(UserController::getAll);
				post(UserController::create);
				path(":userId", () -> {
					get(UserController::getOne);
					patch(UserController::update);
					delete(UserController::delete);
				});
			});

		}).get("/", ctx -> ctx.redirect("swagger-ui")).start(getHerokuAssignedPort());

	}

	private static int getHerokuAssignedPort() {
		ProcessBuilder processBuilder = new ProcessBuilder();
		if (processBuilder.environment().get("PORT") != null) {
			return Integer.parseInt(processBuilder.environment().get("PORT"));
		}
		return 7000;
	}

	private static OpenApiPlugin getConfiguredOpenApiPlugin() {

		Info info = new Info().version("1.0").title("PM-rafaelVentura")
				.description("API Javalin + SonarCloud + Heroku");

		OpenApiOptions options = new OpenApiOptions(info).activateAnnotationScanningFor("edu.unirio.br")
				.path("/swagger-docs") // endpoint for OpenAPI json
				.swagger(new SwaggerOptions("/swagger-ui")) // endpoint for swagger-ui
				.reDoc(new ReDocOptions("/redoc")) // endpoint for redoc
				.defaultDocumentation(doc -> {
					doc.json("500", ErrorResponse.class);
					doc.json("503", ErrorResponse.class);
				});

		return new OpenApiPlugin(options);
	}
}