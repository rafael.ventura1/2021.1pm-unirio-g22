package br.uniriotec.pm.services;

import java.util.Collection;

import org.junit.Test;

import com.google.common.base.Verify;

import br.uniriotec.pm.models.User;

public class UserServiceTest {

	@Test
	public void GET_ALL_USERS_gives_user_collection() {
		Collection<User> allUsers = UserService.getAll();
		Verify.verifyNotNull(allUsers);
	}

}
